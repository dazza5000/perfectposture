package com.brucegiese.perfectposture;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.activeandroid.query.Select;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ValueFormatter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This fragment represents the graph of the Z-axis posture data.
 * It uses a broadcast receiver to get the data in real-time and it uses the database
 * to get any previous data.  It uses MPAndroidChart to plot the data.  A previous
 * pre-beta version used aChartEngine.
 */
public class GraphFragment extends Fragment {
    private static final String TAG = "com.brucegiese.graph";
    private static final int DATA_POINTS_TO_SHOW = 33;

    private LineChart mLineChart;
    private PieChart mPieChart;
    private int mIndex;
    private ArrayList<Entry> mPostureSamples;
    private ArrayList<Entry> mPieChartValues;
    private ArrayList<BarEntry> mBarChartValues;
    private LineData mLineData;
    private PieData mPieData;
    private BarData mBarData;
    private BarChart mBarChart;
    private DataReceiver mDataReceiver;
    private boolean mChartValid = false;
    ArrayList<Integer> mPostureSummary;
    ArrayList<Integer> mCurrentDaySummary;
    Map<Integer, ArrayList<Integer>> mDailySummary;
    private ArrayList<Float> mLineChartPointArray = new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDataReceiver = new DataReceiver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_graph, container, false);

        /*
        *       Set up the chart
         */
        mLineChart = (LineChart) v.findViewById(R.id.chart);
        //mLineChart.setBackgroundColor(getResources().getColor(R.color.neutral_main_color));
        mLineChart.setDescription("");
        mLineChart.setNoDataText(getString(R.string.no_chart_data));
        mLineChart.setDrawGridBackground(true);
        mLineChart.setGridBackgroundColor(getResources().getColor(R.color.neutral_main_color));
        mLineChart.setDrawBorders(false);
        mLineChart.setTouchEnabled(false);




        /*
        *       Y-Axis stuff
         */
        YAxis yAxisRight = mLineChart.getAxisRight();
        yAxisRight.setEnabled(false);

        YAxis yAxis = mLineChart.getAxisLeft();
        yAxis.setTextColor(getResources().getColor(R.color.light_secondary_color));
        yAxis.setEnabled(true);
        yAxis.setDrawAxisLine(true);
        yAxis.setDrawGridLines(true);
        yAxis.setDrawLabels(true);
        yAxis.setDrawTopYLabelEntry(true);
        yAxis.setLabelCount(17);
        yAxis.setStartAtZero(false);
        yAxis.setAxisMaxValue(95);
        yAxis.setAxisMinValue(-95);
        yAxis.setSpaceTop(0.0f);       // leave this much percent space above max value
        yAxis.setSpaceBottom(0.0f);    // leave this much percent space below min value
        yAxis.setEnabled(false);
        drawLimitLines();

        /*
        *       X-Axis stuff
         */
        XAxis xAxis = mLineChart.getXAxis();
        xAxis.setDrawLabels(false);
        xAxis.setEnabled(false);

        /*
        *       Data setup
         */
        setupData();



        // Register the broadcast receiver
        IntentFilter iFilter = new IntentFilter();
        iFilter.addAction(OrientationService.NEW_DATA_POINT_INTENT);
        iFilter.addAction(PerfectPostureActivity.REDRAW_GRAPH_INTENT);
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(mDataReceiver, iFilter);

        /*Setup Pie Chart

         */
        mPieChart = (PieChart) v.findViewById(R.id.piechart);
        //mPieChart.setBackgroundColor(getResources().getColor(R.color.neutral_main_color));
        mPieChart.setDescription("");
        mPieChart.setNoDataText(getString(R.string.no_chart_data));
        mPieChart.setUsePercentValues(true);
        mPieChart.setHoleRadius(40f);
        mPieChart.setDrawHoleEnabled(true);
        mPieChart.animateY(1800);
        mPieChart.setTransparentCircleRadius(0f);
        mPieChart.setTouchEnabled(false);


        mBarChart = (BarChart) v.findViewById(R.id.bar_chart);
        //mBarChart.setBackgroundColor(getResources().getColor(R.color.neutral_main_color));
        mBarChart.setDescription("");
        mBarChart.setNoDataText(getString(R.string.no_chart_data));
        mBarChart.setTouchEnabled(false);
        mBarChart.setPinchZoom(false);
        mBarChart.getAxisRight().setDrawGridLines(false);
        mBarChart.setDrawBarShadow(false);
        YAxis barChartYAxis = mBarChart.getAxisLeft();


        barChartYAxis.setTextColor(getResources().getColor(R.color.light_secondary_color));
        barChartYAxis.setAxisMaxValue(100);
        barChartYAxis.setAxisMinValue(0);
        barChartYAxis.setEnabled(false);

        mBarChart.getAxisRight().setEnabled(false);


        new LoadSummaryChartDataFromDatabase().execute();


        return v;
    }


    private void drawLimitLines() {
        if (mLineChart != null) {
            int posThreshold = OrientationService.getZAxisPositiveThreshold(getActivity());
            int negThreshold = OrientationService.getZAxisNegativeThreshold(getActivity());
            YAxis yAxis = mLineChart.getAxisLeft();
            yAxis.removeAllLimitLines();

            LimitLine zeroLimit = new LimitLine(0f, getString(R.string.best_posture));
            zeroLimit.setLineColor(getResources().getColor(R.color.chart_green));
            zeroLimit.setLineWidth(2f);
            zeroLimit.setTextColor(getResources().getColor(R.color.chart_green));
            zeroLimit.setTextSize(10f);
            zeroLimit.enableDashedLine(10, 10, 0);
            zeroLimit.setLabelPosition(LimitLine.LimitLabelPosition.POS_LEFT);
            yAxis.addLimitLine(zeroLimit);

            LimitLine upperLimit = new LimitLine((float) posThreshold, getString(R.string.max_forward_tilt));
            upperLimit.setLineColor(getResources().getColor(R.color.chart_red));
            upperLimit.setLineWidth(2f);
            upperLimit.setTextColor(getResources().getColor(R.color.chart_red));
            upperLimit.setTextSize(10f);
            upperLimit.enableDashedLine(10, 10, 0);
            upperLimit.setLabelPosition(LimitLine.LimitLabelPosition.POS_LEFT);
            yAxis.addLimitLine(upperLimit);
            LimitLine lowerLimit = new LimitLine((float) negThreshold, getString(R.string.max_backward_tilt));
            lowerLimit.setLineColor(getResources().getColor(R.color.chart_red));
            lowerLimit.setLineWidth(2f);
            lowerLimit.setTextColor(getResources().getColor(R.color.chart_red));
            lowerLimit.setTextSize(10f);
            lowerLimit.enableDashedLine(10, 10, 0);
            lowerLimit.setLabelPosition(LimitLine.LimitLabelPosition.POS_LEFT);
            yAxis.addLimitLine(lowerLimit);
            mLineChart.invalidate();
        }
    }

    /**
     * Set up the data in the fragment.  This can be called to refresh the data if
     * the user clears out the database, for instance.
     */
    protected void setupData() {

        mIndex = 0;
        mLineChartPointArray.clear();
        new LoadFromDatabase().execute();
        mChartValid = true;

    }


    private void setupPieData() {
        PieDataSet mPieDataSet;


        mPieChartValues = new ArrayList<Entry>();

        for (int i = 0; i < (mPostureSummary.size()); i++) {
            mPieChartValues.add(new Entry((float) mPostureSummary.get(i), i));
        }

        mPieDataSet = new PieDataSet(mPieChartValues, "Posture Readings");
        mPieDataSet.setSliceSpace(3f);

        ArrayList<String> xVals = new ArrayList<>();
        xVals.add("Good");
        xVals.add("");

        mPieDataSet.setColors(new int[]{R.color.chart_green, R.color.chart_red}, getActivity());

        mPieData = new PieData(xVals, mPieDataSet);      // PieData is a subclass of ChartData
        mPieData.setValueTextSize(11f);
        mPieData.setValueTextColor(Color.WHITE);
        mPieData.setValueFormatter(new PosturePercentFormatter());
        mPieChart.setData(mPieData);

        Legend pieChartLegend = mPieChart.getLegend();
        pieChartLegend.setEnabled(false);

        mPieChart.invalidate();
    }


    private void setupBarChartData() {

        BarDataSet mBarDataSet;
        mBarChartValues = new ArrayList<BarEntry>();
        float goodPercent;

        //Doing some conversion to convert the aggregate values to percentages - could probably use some refactoring
        for(int currentDay = 0; currentDay <= 6; currentDay++){

            int currentDayTotal = mDailySummary.get(currentDay).get(0) + mDailySummary.get(currentDay).get(1);
            if (currentDayTotal != 0) {
                goodPercent = (((float) mDailySummary.get(currentDay).get(0) / currentDayTotal) * 100);
            } else {
                goodPercent = 0;
            }

            int goodPercentInteger = (int) Math.round(goodPercent);

            //Add the percentage value as a Bar Entry to Bar Chart Values
            mBarChartValues.add(new BarEntry(new float[]{(100 - goodPercentInteger), goodPercentInteger}, currentDay));
        }





        mBarDataSet = new BarDataSet(mBarChartValues, "");
        mBarDataSet.setStackLabels(new String[]{"", "Good Readings"});


        //Create blank x values - an enhacement would be to determine the common name for each of the last 7 days and put
        //that as an x value
        ArrayList<String> xValues = new ArrayList<>();
        xValues.add("");
        xValues.add("");
        xValues.add("");
        xValues.add("");
        xValues.add("");
        xValues.add("");
        xValues.add("");


        mBarDataSet.setColors(new int[]{R.color.neutral_main_color, R.color.chart_green}, getActivity());

        mBarData = new BarData(xValues, mBarDataSet);      // PieData is a subclass of ChartData
        mBarData.setValueTextSize(16f);
        mBarData.setValueTextColor(Color.BLACK);
        mBarData.setValueFormatter(new PosturePercentFormatter());
        mBarChart.setData(mBarData);

        Legend l = mBarChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_RIGHT);
        l.setFormSize(8f);
        l.setFormToTextSpace(4f);
        l.setXEntrySpace(6f);


        mBarChart.invalidate();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mDataReceiver);
        mIndex = 0;
        // Cleanup whatever we can... and hope garbage collection does the rest.
        mChartValid = false;
        mLineChart.clear();
        mLineChart = null;
        mPostureSamples = null;

        mBarChart.clear();
        mBarChart = null;
        mDailySummary = null;

        mPieChart.clear();
        mPieChart = null;
        mPostureSummary = null;



    }

    /**
     * This adds one point of data to the chart.
     * @param value  Z-axis posture value from vertical, Positive means device leaning backward.
     */
    private void loadLineChartValues(List<Sample> values) {

        if (mChartValid) {                          // chart is not valid during rotations etc.



            ArrayList<String> xVals = new ArrayList<String>();
            ArrayList<Entry> mPostureSamples = new ArrayList<Entry>();

            for (int i = 0; i < values.size(); i++) {
                xVals.add("");
                mPostureSamples.add(new Entry(values.get(i).value, i));
            }

            LineDataSet set1 = new LineDataSet(mPostureSamples, "DataSet 1");
            set1.setValueTextSize(0);            // Can't find any other way to disable txt
            set1.setCircleSize(2);
            set1.setDrawCircleHole(true);
            set1.setLineWidth(2);
            set1.setColor(getResources().getColor(R.color.actionbar_background));
            set1.setCircleColor(getResources().getColor(R.color.actionbar_background));
            set1.setDrawCircleHole(true);

            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(set1);

            LineData mNewLineData = new LineData(xVals, dataSets);      // LineData is a subclass of ChartData

            mLineChart.setVisibleXRange(DATA_POINTS_TO_SHOW);
            mLineChart.clear();
            mLineChart.setData(mNewLineData);

            View chart = getActivity().findViewById(R.id.chart);

            chart.setContentDescription("This chart displays real time values");
            mLineChart.invalidate();


             /*
        *       Can't set up legend until the data is set up.
         */
            Legend legend = mLineChart.getLegend();
            legend.setEnabled(false);
            // When clearing data, the legend disappears and the chart re-sizes, looks odd, so don't use

        /*
        *       Chart interactions
         */
            mLineChart.setDragEnabled(true);
            mLineChart.setPinchZoom(false);

        } else {
            Log.d(TAG, "addNewPoint() mChartValid is false");
        }
    }

    /**
     * This AsyncTask loads previous data points from the database.
     */
    private class LoadFromDatabase extends AsyncTask<Void, Void, List<Sample>> {

        /**
         * Get all the points from the database.
         * Unfortunately, "WHERE ROWNUM <= ?" won't work in the ORM we're using.
         * @return                  List of all the Sample objects from the database
         */
        protected List<Sample> doInBackground(Void... x) {
            return new Select()
                    .from(Sample.class)
                    .orderBy("_ID DESC")
                    .limit(DATA_POINTS_TO_SHOW)
                    .execute();
        }

        protected void onPostExecute(List<Sample> values) {
            Collections.reverse(values);

            loadLineChartValues(values);
        }
    }


    private class LoadSummaryChartDataFromDatabase extends AsyncTask<Void, Void, List<Sample>> {

        /**
         * Get all the points from the database.
         * Unfortunately, "WHERE ROWNUM <= ?" won't work in the ORM we're using.
         *
         * @return List of all the Sample objects from the database
         */


        protected List<Sample> doInBackground(Void... x) {
            return new Select()
                    .from(Sample.class)
                    .orderBy("_ID ASC")
                    .execute();

        }

        protected void onPostExecute(List<Sample> values) {

            //Creating an array to hold the timestamps for the last 7 days
            ArrayList<Long> timeArray = new ArrayList<>();

            Calendar sevenDaysAgo = getSevenDaysAgo();

            mDailySummary = new HashMap<>();

            //Populating time array with timestamps for the last 7 days and populating hashmap to hold daily summaries
            for (int x = 0; x <= 6; x++) {

                sevenDaysAgo.add(Calendar.DAY_OF_MONTH, 1);

                Log.d(TAG, "The timeArray timestamp entry for sevenDays ago is"
                        + sevenDaysAgo.getTimeInMillis());

                timeArray.add(x, (sevenDaysAgo.getTimeInMillis()));

                Log.d(TAG, "The timeArray timestamp entry at " + x + " is " + timeArray.get(x));


                mDailySummary.put(x, new ArrayList<Integer>());
                mDailySummary.get(x).add(0);
                mDailySummary.get(x).add(0);

            }

            if (values.size() > 0) {

                mPostureSummary = new ArrayList<>();
                mPostureSummary.add(0);
                mPostureSummary.add(0);


                int mCurrentDayCounter = 0;


                for (Sample s : values) {

                    if (s.isGoodPosture()) {
                        Integer positiveValue = mPostureSummary.get(0);
                        positiveValue += 1;
                        mPostureSummary.set(0, positiveValue);
                    } else {
                        Integer negativeValue = mPostureSummary.get(1);
                        negativeValue += 1;
                        mPostureSummary.set(1, negativeValue);
                    }

                    if(s.getDate().getTime() >= timeArray.get(6)){

                        if (s.isGoodPosture()) {
                            Integer positiveValue = mDailySummary.get(6).get(0);
                            positiveValue += 1;
                            mDailySummary.get(6).set(0, positiveValue);
                        } else {
                            Integer negativeValue = mDailySummary.get(6).get(1);
                            negativeValue += 1;
                            mDailySummary.get(6).set(1, negativeValue);
                        }

                    } else if(timeArray.get(mCurrentDayCounter) <= s.getDate().getTime() &&
                            s.getDate().getTime() < timeArray.get(mCurrentDayCounter+1)) {

                        if (s.isGoodPosture()) {
                            Integer positiveValue = mDailySummary.get(mCurrentDayCounter).get(0);
                            positiveValue += 1;
                            mDailySummary.get(mCurrentDayCounter).set(0, positiveValue);
                        } else {
                            Integer negativeValue = mDailySummary.get(mCurrentDayCounter).get(1);
                            negativeValue += 1;
                            mDailySummary.get(mCurrentDayCounter).set(1, negativeValue);
                        }

                    } else if (s.getDate().getTime() >= timeArray.get(mCurrentDayCounter+1)){
                        //When starting a new day reset the current day arraylist
                        mCurrentDayCounter +=1;
                    }




                }

                setupPieData();
                setupBarChartData();

            }

        }
    }


        /**
         * Receive new data points as they're being added to the database.
         */
        class DataReceiver extends BroadcastReceiver {

            @Override
            public void onReceive(Context c, Intent i) {
                if (i.getAction().equals(OrientationService.NEW_DATA_POINT_INTENT)) {
                    int value = i.getIntExtra(OrientationService.EXTRA_VALUE, Orientation.IMPOSSIBLE_INTEGER);
                    setupData();
                    //setupData();
                } else if (i.getAction().equals(PerfectPostureActivity.REDRAW_GRAPH_INTENT)) {
                    drawLimitLines();
                } else {
                    Log.e(TAG, "Received an unexpected broadcast intent");
                }

            }
        }

        protected void refreshSummaryData() {

            Runnable runnable = new Runnable() {
                public void run() {
                    Log.i(TAG, "Runnable executing.");
                    new LoadSummaryChartDataFromDatabase().execute();

                }
            };

            new Handler().postDelayed(runnable, 333);
        }


        protected void resetPieData() {

        mPostureSummary = new ArrayList<>();
        mPostureSummary.add(1);
        mPostureSummary.add(0);

        mDailySummary = new HashMap<>();


        //Populating time array with timestamps for the last 7 days and populating hashmap to hold daily summaries
        for (int x = 0; x <= 6; x++) {

            mDailySummary.put(x, new ArrayList<Integer>());
            mDailySummary.get(x).add(0);
            mDailySummary.get(x).add(0);

        }

            mDailySummary.get(6).set(0, 1);
            mDailySummary.get(6).set(1, 0);

            setupPieData();
            setupBarChartData();

        }



        private Calendar getSevenDaysAgo() {

            Calendar date = new GregorianCalendar();

            Log.d(TAG, "Current timestamp is: " + date.getTimeInMillis());

            // reset hour, minutes, seconds and millis
            date.set(Calendar.HOUR_OF_DAY, 0);
            date.set(Calendar.MINUTE, 0);
            date.set(Calendar.SECOND, 0);
            date.set(Calendar.MILLISECOND, 0);

            Log.d(TAG, "Midnight timestamp of today is: " + date.getTimeInMillis());

            // midnight 7 days ago
            date.add(Calendar.DAY_OF_MONTH, -7);

            Log.d(TAG, "Midnight timestamp from 7 days ago is: " + date.getTimeInMillis());

            return date;


        }

    public class PosturePercentFormatter implements ValueFormatter {

        protected DecimalFormat mFormat;

        public PosturePercentFormatter() {
            mFormat = new DecimalFormat("###");
        }

        @Override
        public String getFormattedValue(float value) {
            return mFormat.format(value) + " %";
        }
    }


}
