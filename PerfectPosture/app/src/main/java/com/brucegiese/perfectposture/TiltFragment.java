package com.brucegiese.perfectposture;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * This fragment allows the user to start and stop the posture measurement service.
 */
public class TiltFragment extends Fragment {
    private static final String TAG = "com.brucegiese.tilt";
    private View mView;
    private CheckStatusReceiver mCheckStatusReceiver;


    public TiltFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCheckStatusReceiver = new CheckStatusReceiver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_tilt, container, false);


        // Register the broadcast receiver
        IntentFilter iFilter = new IntentFilter();
        iFilter.addAction(OrientationService.CHECK_STATUS_INTENT);
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(mCheckStatusReceiver, iFilter);
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // In case of multiple instances running and other complex scenarios.

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // Un-register the broadcast receiver
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mCheckStatusReceiver);

        mView = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // If the service is not doing orientation, then stop the whole service
        if( ! OrientationService.sIsRunning) {
            Log.d(TAG, "onDestroy(): Stopping the service");
            Intent intent = new Intent(getActivity(), OrientationService.class);
            getActivity().stopService(intent);
        }
    }

    /**
     * The service can be stopped by other means, so it will tell us when that happens.
     */
    class CheckStatusReceiver extends BroadcastReceiver {

        public CheckStatusReceiver() { }

        @Override
        public void onReceive(Context c, Intent i) {
            if( i.getAction().equals(OrientationService.CHECK_STATUS_INTENT)) {
                Log.e(TAG, "Received a check status intent");
            } else {
                Log.e(TAG, "Received an unexpected broadcast intent");
            }

        }
    }
}
