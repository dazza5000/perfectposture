package com.brucegiese.perfectposture;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by daz on 5/13/15.
 */
public class ScreenStateBroadcastReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = "SSBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent serviceIntent = new Intent(context, OrientationService.class);

        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {

            Log.i(LOG_TAG, "Screen went OFF");
            serviceIntent.setAction(OrientationService.STOP_CHECKING_ACTION);


        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Log.i(LOG_TAG, "Screen went ON");
            serviceIntent.setAction(OrientationService.START_CHECKING_ACTION);

        }

        context.startService(serviceIntent);


    }




}
