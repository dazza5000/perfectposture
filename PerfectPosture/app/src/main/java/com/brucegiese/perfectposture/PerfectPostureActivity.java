package com.brucegiese.perfectposture;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.activeandroid.query.Delete;


/**
 * This displays the orientation of the device with respect to gravity.
 * It can be enabled/disabled via a big_checkbox and has configurable
 * parameters as implemented in TiltFragment.
 *
 */
public class PerfectPostureActivity extends Activity {
    private final static String TAG = "com.brucegiese.perfpost";
    public final static String REDRAW_GRAPH_INTENT = "com.brucegiese.perfectposture.redraw";
    private final static int INTRO_CONTROL_PAGE = 0;
    private final static int DATA_PAGE = 1;
    private final static int SETTINGS_PAGE = 2;
    private final static int LAST_PAGE_NUM = SETTINGS_PAGE;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private int mPosition;
    private GraphFragment mGraphFragment;

    @Override
    protected void onResume() {
        super.onResume();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(OrientationService.POSTURE_NOTIFICATION_ID);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfect_posture);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(LAST_PAGE_NUM+1);

        viewPager.setOnPageChangeListener( new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected( int position) {     // pages are zero based
                if (mPosition == 2 && position == 1) {
                    // We changed from settings page to the chart page.  Settings may have changed
                    // Tell the GraphFragment to redraw the red dotted lines, which may change
                    Intent bcastIntent = new Intent(REDRAW_GRAPH_INTENT);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(bcastIntent);
                }
                mPosition = position;
            }

            @Override public void onPageScrollStateChanged(int arg0){
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

        });

        viewPager.setAdapter(mSectionsPagerAdapter);

        final PagerTabStrip pts = (PagerTabStrip) findViewById(R.id.pager_tab_strip);
        pts.setTextColor(getResources().getColor(R.color.medium_secondary_color));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSectionsPagerAdapter = null;
    }

    /**
     * Pager adapter for main activity which is just a ViewPager for the small
     * number of pages we need for things like configuration, the chart, saved
     * data.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch( position ) {

                case INTRO_CONTROL_PAGE:
                    return new TiltFragment();

                case DATA_PAGE:
                    return new GraphFragment();

                case SETTINGS_PAGE:
                    return new SettingsFragment();

                default:
                    Log.e(TAG, "Internal error: getItem() position out of range: " + position);
                    return null;
            }
        }

        @Override
        public int getCount() {
            return LAST_PAGE_NUM + 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case INTRO_CONTROL_PAGE:
                    return getString(R.string.page_title_intro).toUpperCase();

                case DATA_PAGE:
                    return getString(R.string.page_title_data).toUpperCase();

                case SETTINGS_PAGE:
                    return getString(R.string.page_title_settings).toUpperCase();

                default:
                    return "INTERNAL ERROR";

            }
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.graph_options, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_item_clear_data:
                // Clear all the data from the database (and also the chart)
                Log.d(TAG, "clearing data");
                new Delete().from(Sample.class).execute();      // delete all records!!!
                mGraphFragment = (GraphFragment) mSectionsPagerAdapter.getRegisteredFragment(DATA_PAGE);
                if (mGraphFragment != null) {
                    mGraphFragment.setupData();
                    mGraphFragment.resetPieData();

                }
                return true;

            case R.id.menu_item_toggle_service:
                Intent intent = new Intent(this, OrientationService.class);
                if (OrientationService.sIsRunning) {
                    intent.setAction(OrientationService.TURN_OFF_SERVICE_ACTION);

                } else {
                    intent.setAction(OrientationService.TURN_ON_SERVICE_ACTION);

                }

                startService(intent);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    this.invalidateOptionsMenu();
                return true;

            case R.id.menu_item_refresh_charts:
                // Clear all the data from the database (and also the chart)
                Log.d(TAG, "refreshing charts");
                mGraphFragment = (GraphFragment) mSectionsPagerAdapter.getRegisteredFragment(DATA_PAGE);
                if (mGraphFragment != null) {
                    mGraphFragment.setupData();
                    mGraphFragment.refreshSummaryData();
                }

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);



        MenuItem toggleItem = menu.findItem(R.id.menu_item_toggle_service);
        if (OrientationService.sIsRunning){
            toggleItem.setTitle(R.string.stop_tilt_detection);
        } else{
            toggleItem.setTitle(R.string.start_tilt_detection);

        }

        return true;
    }
}
